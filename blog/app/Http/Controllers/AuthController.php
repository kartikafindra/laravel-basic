<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function register()
    {
        return view('register');
    }
    public function welcome(Request $request)
    {
        $nama = $request->input('firstname');
        $nama2 = $request->input('lastname');
        return view('welcome', ['nama'=> $nama, 'nama2'=>$nama2]);
    }
    
}
